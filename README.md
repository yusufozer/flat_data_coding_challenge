# Coding challenge

## Created with

- Python 2.7.13
- pandas 0.23.4


## Instructions to run

You have to install requirements before start; 
```bash
$ pip install -r requirements.txt
```
___
If `pricat.csv` , `mappings.csv` , `mappings2.csv` and `processor.py` files exist in same directory with `sample.py` you can execute it, then the result output gonna be printed to console and to `output.json` file; 
```bash
$ python ./sample.py
```
___
You can run tests with coverage;
```bash
$ coverage run tests.py
$ coverage report -m
```
or without coverage;
```bash
$ python ./tests.py
```
