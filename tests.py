from processor import FlatDataProcessor
from pandas.compat import StringIO
import unittest
import json


class TestFlatDataProcessor(unittest.TestCase):
    def setUp(self):
        self.pricat = FlatDataProcessor()
        self.pricat.set_default_separator(';')
        self.combine_options = {
            'size': ['product', 'size'],  # same column as target
            'price_currency': ['price', 'currency'],  # add new column, check multiple options
        }
        self.structure = ['group', 'product', 'color']
        self.data_csv = StringIO("""brand;group;product;color;size;price;currency
panda;sandals;15189;1;36;10;USD
panda;sandals;15189;2;36;10;USD
panda;sandals;15189;1;37;10;USD
panda;sandals;15189;2;37;10;USD
panda;shoes;25189;1;36;60;EUR
panda;shoes;25189;2;36;60;EUR
panda;shoes;25189;1;37;60;EUR
panda;shoes;25189;2;37;70;EUR""")
        self.map_csv = StringIO("""source;destination;source_type;destination_type
panda;Panda;brand;brand
1;White;color;color_name
White|36;White size 36;color_name|size;size_color""")
        self.questionmark_data = StringIO("""brand?group?color?size
panda?sandals?1?36
panda?sandals?2?36""")
        self.questionmark_map = StringIO("""source?destination?source_type?destination_type
1?Blue?color?color_name
2?Orange?color?color_name""")

    def test_set_separator_and_read_csv(self):
        """Test default separator feature"""
        self.pricat.set_default_separator('?')
        self.pricat.read_csv(self.questionmark_data)
        self.assertEqual((2, 4), self.pricat.data.shape)  # read_csv uses default separator
        self.pricat.apply_map_csv(self.questionmark_map)
        self.assertEqual((2, 5), self.pricat.data.shape)  # apply_map_csv uses default separator
        self.assertTrue('?' in self.pricat.to_csv())  # to_csv uses default separator
        self.pricat.read_csv(self.data_csv, sep=';')
        self.assertEqual((8, 7), self.pricat.data.shape)  # second change on default separator

    def test_apply_map_csv(self):
        """Test apply map file function"""
        self.pricat.read_csv(self.data_csv)
        self.pricat.apply_map_csv(self.map_csv)
        self.assertEqual((8, 9), self.pricat.data.shape)
        self.assertEqual('Panda', self.pricat.data.get('brand').iloc[0])
        self.assertEqual('Panda', self.pricat.data.get('brand').iloc[7])
        self.assertIn('color_name', self.pricat.data.columns)
        self.assertEqual('White', self.pricat.data.get('color_name').iloc[0])
        self.assertTrue(self.pricat.data.get('color_name').isna().iloc[3])
        self.assertEqual('White size 36', self.pricat.data.get('size_color').iloc[0])
        self.assertTrue(self.pricat.data.get('size_color').isna().iloc[1])

    def test_combine_fields(self):
        """Test combine fields from options function"""
        self.pricat.read_csv(self.data_csv)
        self.pricat.combine_fields(self.combine_options)
        self.assertEqual((8, 8), self.pricat.data.shape)
        self.assertNotIn('size_color', self.pricat.data.columns)
        self.assertEqual('10 USD', self.pricat.data.get('price_currency').iloc[3])
        self.assertEqual('15189 36', self.pricat.data.get('size').iloc[0])
        self.assertEqual('25189 37', self.pricat.data.get('size').iloc[7])
        self.combine_options['new_column'] = ['fake_column', 'currency']
        with self.assertRaises(ValueError):
            self.pricat.combine_fields(self.combine_options)

    def test_combine_fields_and_apply_map_csv(self):
        """Test combine fields function and multiple apply_map_csv together"""
        self.pricat.read_csv(self.data_csv)
        self.pricat.apply_map_csv(self.map_csv)
        self.pricat.combine_fields(self.combine_options)
        self.pricat.apply_map_csv(self.questionmark_map, sep='?')
        self.assertEqual((8, 10), self.pricat.data.shape)
        self.assertIn('size', self.pricat.data.columns)
        self.assertEqual('Blue', self.pricat.data.get('color_name').iloc[0])
        self.assertEqual('Orange', self.pricat.data.get('color_name').iloc[1])
        self.assertEqual('10 USD', self.pricat.data.get('price_currency').iloc[3])
        self.assertTrue(self.pricat.data.get('size_color').isna().iloc[1])

    def test_to_json(self):
        """Test to json feature"""
        self.pricat.read_csv(self.data_csv)
        self.pricat.apply_map_csv(self.map_csv)
        self.pricat.combine_fields(self.combine_options)
        self.assertEqual((8, 10), self.pricat.data.shape)
        decoded_json = json.loads(self.pricat.to_json(self.structure))
        self.assertEqual('Panda', decoded_json.get('brand'))
        self.assertNotEqual('White', decoded_json.get('color_name'))
        self.assertEqual(2, len(decoded_json.get('groups')))
        self.assertEqual('10 USD', decoded_json.get('groups')[0].get('price_currency'))
        self.assertIsNone(decoded_json.get('groups')[1].get('price_currency', None))
        self.assertEqual(2, len(decoded_json.get('groups')[0].get('products')[0].get('colors')))
        self.assertEqual(25189, decoded_json.get('groups')[1].get('products')[0].get('product'))
        self.assertEqual('White', decoded_json.get('groups')[1].get('products')[0].get('colors')[0].get('color_name'))
        self.assertIsNone(decoded_json.get('groups')[1].get('products')[0].get('colors')[1].get('color_name'))


if __name__ == '__main__':
    unittest.main()
