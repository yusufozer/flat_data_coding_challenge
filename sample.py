from processor import FlatDataProcessor
import json

pricat = FlatDataProcessor()

pricat.set_default_separator(';')

pricat.read_csv('pricat.csv')

pricat.apply_map_csv('mappings.csv')

pricat.apply_map_csv('mappings2.csv')

combine_options = {
    'price_buy_net_currency': ['price_buy_net', 'currency'],
}

pricat.combine_fields(combine_options)

structure = ['article_number', 'ean']

json_str = pricat.to_json(structure)

print(json_str)

python_dict = pricat.to_dict(structure)
with open('output.json', 'w') as file:
    json.dump(python_dict, file, indent=4)
