import pandas as pd
import json


class FlatDataProcessor(object):
    """
    Apply mappings from CSV files and/or combine columns from options to a Flat CSV file.
    Can returns nested Json or CSV or Pandas's DataFrame.
    """
    default_separator = ','

    def __init__(self):
        self.data = None

    def set_default_separator(self, separator):
        """
        You can change default separator used while input/output.
        """
        self.default_separator = separator

    def _check_kwargs(self, **kwargs):
        kwargs['sep'] = self.default_separator if 'sep' not in kwargs else kwargs['sep']
        return kwargs

    def read_csv(self, file_path_or_buffer, **kwargs):
        """
        Read CSV (comma-separated) file into DataFrame. Pandas's DataFrame read_csv method options can be used.
        """
        kwargs = self._check_kwargs(**kwargs)
        self.data = pd.read_csv(file_path_or_buffer, **kwargs)

    def apply_map_csv(self, file_path_or_buffer, **kwargs):
        """
        Applies mappings from a source field to a destination field by reading CSV file.
        Pandas's DataFrame read_csv method options can be used.
        Multiple source values (separated with pipe char), can be combined into a new destination field in the map file.

        Mapping file should be like this:
            source;destination;source_type;destination_type
            EU|38;European size 38;size_group_code|size_code;size
            1;Nero;color_code;color
            2;Marrone;color_code;color
        """
        kwargs = self._check_kwargs(**kwargs)
        mappings = pd.read_csv(file_path_or_buffer, **kwargs)
        for key, groupset in mappings.groupby(['source_type', 'destination_type']):  # Apply mappings
            if '|' in key[0]:  # Multiple columns combined
                source_types = key[0].split('|')
                for rule in groupset.itertuples():
                    self.data.loc[self.data.eval(
                        ' & '.join(["{} == '{}'".format(k, v) for k, v in zip(source_types, rule.source.split('|'))])
                    ), key[1]] = rule.destination
            else:  # Single column
                self.data[key[1]] = self.data[key[0]].map(
                    dict(zip(groupset.source.astype(int, errors='ignore').tolist(), groupset.destination)))

    def combine_fields(self, options):
        """
        Applies configurable options to combine multiple fields into a new field.

        Sample options:
            {
                'new_column_name': ['column_exist_1', 'column_exist_2'],
                'price_buy_net_currency': ['price_buy_net', 'currency'],
            }
        """
        for new_field_name, fields_to_combine in options.items():
            new_field_value = ''
            for field in fields_to_combine:
                try:
                    new_field_value += (' ' if len(new_field_value) else '') + self.data[field].map(str)
                except (KeyError, ):
                    raise ValueError('Column "{}" not exist in data!'.format(field))
            self.data[new_field_name] = new_field_value

    def _handle_attributes(self, frame, attributes):
        """
        Set attributes that are common for frame, returns element and different attributes
        """
        element = {}
        different_attributes = []
        for attribute in attributes:
            unique_value_count = frame[attribute].nunique(dropna=False)
            if unique_value_count == 1:  # Same value for all
                element[attribute] = frame.get(attribute).iloc[0] if not frame.get(attribute).isna().iloc[0] else None
            elif unique_value_count > 1:  # Columns that are empty should not be copied, exclude empty columns
                different_attributes.append(attribute)
        return element, different_attributes

    def _builder(self, different_attributes, structure, frame, counter=0):
        """
        Build nested dictionary according to structure levels to export data to json.
        """
        items = []
        for item_value, group in frame.groupby([structure[counter]]):
            item, child_attrs = self._handle_attributes(group, different_attributes)
            item[structure[counter]] = item_value
            next_counter = counter + 1
            if len(structure) > next_counter:  # if not last level, call recursive
                item['{}s'.format(structure[next_counter])] = self._builder(child_attrs, structure, group, next_counter)
            items.append(item)
        return items

    def to_json(self, structure, indent=4):
        """
        Returns nested json with moving attributes that are common for all childrens,
        up to their parent according to sorted column names in desired structure list.

        Sample structure:
            ['group_id', 'product_id', 'variation_id']
        """
        return json.dumps(self.to_dict(structure), indent=indent)

    def to_dict(self, structure):
        """
        Returns data as nested python dict with moving attributes that are common for all childrens,
        up to their parent according to sorted column names in desired structure list.

        Sample structure:
            ['group_id', 'product_id', 'variation_id']
        """
        catalog, different_attributes = self._handle_attributes(self.data, self.data.keys())  # Catalog level commons
        catalog['{}s'.format(structure[0])] = self._builder(different_attributes, structure, self.data)
        return catalog

    def to_csv(self, **kwargs):
        """
        Returns flat csv data as flat csv data. Pandas's DataFrame to_csv method options can be used.
        """
        kwargs = self._check_kwargs(**kwargs)
        return self.data.to_csv(**kwargs)
